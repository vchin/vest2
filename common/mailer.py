import os
import six
import logging

from email.mime.image import MIMEImage
from django.core.mail import EmailMultiAlternatives

logger = logging.getLogger('vest')

def mail_send(to, message_txt, **kwargs):
    """Base partner mail function

    Args:
        to(str or list): Receiver mail.
        message_txt: Default email message text.

    Keyword Args:
        subject: Email subject.
        message_html: HTML message, for new email.
        email_from: Sender email.
        images(list): Embedded email png images.
    """
    from django.conf import settings

    subject = kwargs.get('subject', 'subject')
    message_html = kwargs.get('message_html', message_txt)
    email_from = kwargs.get('email_from', settings.DEFAULT_FROM_EMAIL)
    email_to = [to] if isinstance(to, six.string_types) else to

    msg = EmailMultiAlternatives(subject, message_txt, email_from, email_to)
    msg.attach_alternative(message_html, 'text/html')

    # array of paths to png images
    images = kwargs.get('images', None)

    if images:
        msg.mixed_subtype = 'related'

        for f in images:
            name = os.path.basename(f).lower()
            with open(f, 'rb') as fp:
                mime_img = MIMEImage(fp.read())
                mime_img.add_header('Content-ID', '<{}>'.format(name))
                msg.attach(mime_img)

    try:
        msg.send()
    except Exception, e:
        logger.debug('mail not sent {} {}'.format(to, message_txt))