import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rxcore.settings")

from django.test.runner import DiscoverRunner

# No DB destroy
class RxDiscoverRunner(DiscoverRunner):
    """
    A Django test runner that uses unittest2 test discovery.
    """
    def run_tests(self, test_labels, extra_tests=None, **kwargs):
        return super(RxDiscoverRunner, self).run_tests(test_labels=test_labels, extra_tests=extra_tests, **kwargs)

    def setup_databases(self, **kwargs):
        return None

    def setup_test_environment(self, **kwargs):
        super(RxDiscoverRunner, self).setup_test_environment(**kwargs)

        import jinja2
        from django.test.signals import template_rendered

        render = jinja2.Template.render

        def new_render(self, *args, **kwargs):
            template_rendered.send(sender=self, template=self, context=self.new_context(dict(*args, **kwargs)))
            return render(self, *args, **kwargs)


        jinja2.Template.render = new_render

    def teardown_databases(self, old_config, **kwargs):
        """
        Destroys all the non-mirror databases.
        """
        # old_names, mirrors = old_config
        # for connection, old_name, destroy in old_names:
        # if destroy:
        #         connection.creation.destroy_test_db(old_name, self.verbosity)

        # def teardown_test_environment(self, **kwargs):
        #     unittest.removeHandler()
        #     teardown_test_environment()



# def run_tests(test_labels, verbosity=1, interactive=False, extra_tests=[],
#               **kwargs):
#     """
#     Run the unit tests for all the test labels in the provided list.
#     Labels must be of the form:
#     - app.TestClass.test_method
#       Run a single specific test method
#     - app.TestClass
#       Run all the test methods in a given class
#     - app
#       Search for doctests and unittests in the named application.
#
#     When looking for tests, the test runner will look in the models and
#     tests modules for the application.
#
#     A list of 'extra' tests may also be provided; these tests
#     will be added to the test suite.
#
#     Returns the number of tests that failed.
#     """
#     options = {
#     'verbosity': verbosity,
#     'interactive': interactive
#     }
#     options.update(kwargs)
#     TeamcityServiceMessages(sys.stdout).testMatrixEntered()
#     if VERSION[1] > 1:
#     return DjangoTeamcityTestRunner(**options).run_tests(test_labels,
#                                                          extra_tests=extra_tests, **options)
#
#     return run_the_old_way(extra_tests, options, test_labels, verbosity)