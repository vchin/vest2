import urllib

import math
import six

from django.http import JsonResponse
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import resolve
from django.http import Http404, HttpResponse
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.views.generic import View
from django.conf import settings

from . import utils
from .utils import ujson_response
from .models import SiteSettings

import logging

logger = logging.getLogger('vest')
try:
    from .. import version
except Exception:
    class version(object):
        VEST_VERSION = 2.1


class BaseMixin(View):
    breadcrumbs = []
    ajax_raw = False

    def permission_check(self, request, *args, **kwargs):
        return None

    def get_breadcrumbs(self):
        return self.breadcrumbs

    def render_to_response(self, context, **response_kwargs):
        context['breadcrumbs'] = self.get_breadcrumbs()

        return self.response_class(
            request=self.request,
            template=self.get_template_names(),
            context=context,
            **response_kwargs
        )

    def get_template_names(self):
        if hasattr(self, 'template_name') and self.template_name:
            tn = self.template_name
        else:
            namespace = resolve(self.request.path).namespace
            base = utils.camel_to_underline(self.__class__.__name__)

            version = getattr(settings, 'VEST_VERSION', None)
            if version is None:
                version = getattr(version, 'VEST_VERSION', 1)

            if version > 2.4:
                tn = 'appls/%s/%s%s' % (namespace, base, getattr(settings,
                                                                 'DEFAULT_TEMPLATE_EXT',
                                                                 '.html'))
            elif version > 2:
                tn = 'apps/%s/%s%s' % (namespace, base, getattr(settings,
                                                                'DEFAULT_TEMPLATE_EXT',
                                                                '.html'))
            else:
                tn = '%s/%s%s' % (namespace, base,
                                  getattr(settings, 'DEFAULT_TEMPLATE_EXT',
                                          '.html'))

        if self.request.is_ajax():
            tna = tn.split('.')
            tna[-2] = '%s_ajax' % tna[-2]
            return ['.'.join(tna)]

        return [tn]

    def _json_response(self, *args, **kwargs):
        return ujson_response(*args, **kwargs)

    def _pre_init(self, request, *args, **kwargs):
        pass

    def _post_init(self, request, *args, **kwargs):
        pass

    def dispatch(self, request, *args, **kwargs):
        result = self.permission_check(request, *args, **kwargs)

        self.paginator_data = {}

        if not result is None:
            return result

        if 'pk' in request.REQUESTS:
            kwargs['pk'] = request.REQUESTS

        if 'slug' in request.REQUESTS:
            kwargs['slug'] = request.REQUESTS['slug']

        self._pre_init(request, *args, **kwargs)

        if 'cmd' in request.REQUESTS:
            try:
                cmd = request.REQUESTS['cmd']

                if not isinstance(cmd, six.string_types):
                    cmd = request.REQUESTS['cmd'][0]

                cmd = cmd.lower()

                handler = getattr(self, 'cmd_%s' % cmd)
                result = handler(request, *args, **kwargs)
            except Exception, e:
                if request.is_ajax():
                    return self._json_response(
                        {'success': False, 'data': None, 'errors': e})
                else:
                    raise Http404(e)

        else:
            result = super(BaseMixin, self).dispatch(request, *args, **kwargs)

        # for ajax response
        if request.is_ajax() or 'cmd' in request.REQUESTS:
            if isinstance(result, TemplateResponse):
                if not result.is_rendered:
                    result.render()
                success = True
                data = result.rendered_content
            elif isinstance(result, JsonResponse):
                return result
            elif isinstance(result, (list, tuple)):
                success, data = result
            else:
                success = result
                data = None

            errors = getattr(self, 'ajax_errors', None)
            if not errors is None and len(errors):
                success = False

            if self.ajax_raw:
                return success

            return self._json_response({'success': success,
                                        'data': data,
                                        'meta': getattr(self, 'meta', {}),
                                        'errors': errors})

        return result


class AuthMixin(BaseMixin):
    no_access_url = None

    def _on_access_granted(self, request, *args, **kwargs):
        pass

    def permission_check(self, request, *args, **kwargs):
        # if not (request.user.is_authenticated() and request.user.is_active and request.user.is_approved):
        if not (request.user.is_authenticated() and request.user.is_active):
            if self.no_access_url is None:
                raise PermissionDenied

            return redirect(self.no_access_url)

        self._on_access_granted(request, *args, **kwargs)


class ViewRobots(View):
    def dispatch(self, request, *args, **kwargs):
        return HttpResponse(SiteSettings.robots_get(),
                            content_type='text/plain')


class Paginator(object):
    @classmethod
    def request_params_populate(cls, request, name_offset='offset',
                                name_limit='limit',
                                limit=10, offset=0):
        params = {'offset': 0, 'limit': 0}

        try:
            params['offset'] = int(request.REQUESTS.get(name_offset, offset))
            params['limit'] = int(request.COOKIES.get(name_limit, limit))
        except Exception, e:
            logger.debug(e)

        return params

    def __init__(self,
                 num_rows,
                 offset=0,
                 limit=10,
                 view_num=11,
                 splitter='...',
                 view_one_page=False,
                 min_limit=10,
                 max_limit=50,
                 name_offset='offset',
                 name_limit='limit',
                 request=None,
                 map_page=None,
                 params=None):
        self.num_rows = num_rows

        if request:
            request_params = Paginator.request_params_populate(request)
            offset = request_params['offset']
            limit = request_params['limit']

        if limit < min_limit:
            limit = min_limit

        if limit > max_limit:
            limit = max_limit

        if offset > num_rows:
            offset = num_rows - limit

        if offset < 0:
            offset = 0

        self.num_rows = num_rows

        self.offset = offset
        self.limit = limit
        self.view_num = view_num
        self.splitter = splitter
        self.view_one_page = view_one_page
        self.num_pages = int(math.ceil(float(num_rows) / limit))
        self.curr_page = int(math.ceil(offset / limit))
        self.view_one_page = view_one_page

        if self.curr_page >= self.num_pages:
            self.curr_page = self.num_pages - 1

        self.next_page = self.curr_page + 1
        if self.next_page >= self.num_pages:
            self.next_page = self.num_pages - 1

        self.prev_page = self.curr_page - 1
        if self.prev_page < 0:
            self.prev_page = 0

        self.offset_next_page = limit * self.next_page
        self.offset_prev_page = limit * self.prev_page
        self.offset_last_page = limit * (self.num_pages - 1)

        self.curr_page += 1
        self.next_page += 1
        self.prev_page += 1

        self.name_offset = name_offset
        self.name_limit = name_limit
        self.map_page = map_page

        if params and self.name_offset in params:
            params = dict(params)
            del params[self.name_offset]

        self.params = params

        if self.params:
            # self.params_json = ',%s' % json.dumps(self.params, sort_keys=True, indent=2)[1:-1]
            items = {}
            for key, val in self.params.iteritems():
                if isinstance(val, str) or isinstance(val, unicode):
                    items[key] = val.encode('utf-8')
                else:
                    items[key] = val
            self.params_urlencoded = '&%s' % urllib.urlencode(items)
        else:
            self.params_json, self.params_urlencoded = '', ''

        if self.map_page:
            if self.next_page in self.map_page:
                self.next_page_data = self.map_page[self.next_page]

            if self.prev_page in self.map_page:
                self.prev_page_data = self.map_page[self.prev_page]

        self.is_last = (self.offset + self.limit) >= self.num_rows

        self.page_range = self._get_page_range()

    def _get_page_range(self):
        num_pages = self.num_pages
        splitter = {'is_splitter': True, 'splitter': self.splitter}
        view_num = self.view_num
        curr_page = self.curr_page
        limit = self.limit

        if num_pages <= view_num:
            pages = [[i, (i - 1) * limit] for i in xrange(1, num_pages + 1)]
            # pages = [[i, (i - 1) * num_page] for i in xrange(1, num_pages + 1)]
        else:
            df = self.view_num / 2
            pages = []
            if curr_page <= df:
                pages.extend(
                    [[i, (i - 1) * limit] for i in xrange(1, view_num)])
                pages.append(splitter)
            elif curr_page >= num_pages - df:
                pages.append([1, 0])
                pages.append(splitter)
                # pages.extend(range(num_pages - viewed_num + 2, num_pages ))

                pages.extend([[i, (i - 1) * limit] for i in
                              xrange(num_pages - view_num + 2, num_pages)])

            else:
                pages.append([1, 0])
                if curr_page != df + 1:
                    pages.append(splitter)
                    #                pages.extend(range(curr_page - df + 1, curr_page + df))
                pages.extend([[i, (i - 1) * limit] for i in
                              xrange(curr_page - df + 1, curr_page + df)])

                if curr_page != num_pages - df:
                    pages.append(splitter)

            pages.append([num_pages, (num_pages - 1) * limit])

        if self.map_page:
            for page in pages:
                if page[0] in self.map_page:
                    page.append(self.map_page[page[0]])

        return pages
        #    page_range = property(_get_page_range)


class VTPaginator(dict):
    def __init__(self, num_rows, **kwargs):
        defaults = {
            'offset': 0,
            'limit': 10,
            'view_num': 11,
            'splitter': '...',
            'view_one_page': False,
            'min_limit': 10,
            'max_limit': 50,
            'params': None,
            'offset_name': 'offset',
            'is_range': False,
            'is_only_ajax' : False,
            'url_params': ''
        }

        defaults.update(kwargs)
        self.update(defaults)

        self['num_rows'] = num_rows

        if self['limit'] < self['min_limit']:
            self['limit'] = self['min_limit']

        if self['limit'] > self['max_limit']:
            self['limit'] = self['max_limit']

        if self['offset'] > self['num_rows']:
            self['offset'] = self['num_rows'] - self['limit']

        if self['offset'] < 0:
            self['offset'] = 0

        self['num_pages'] = int(math.ceil(float(self['num_rows'])
                                          / self['limit']))

        self['curr_page'] = int(math.ceil(self['offset'] / self['limit']))

        self['next_page'] = self['curr_page'] + 1
        if self['next_page'] >= self['num_pages']:
            self['next_page'] = self['num_pages'] - 1

        self['prev_page'] = self['curr_page'] - 1
        if self['prev_page'] < 0:
            self['prev_page'] = 0

        self['offset_next_page'] = self['limit'] * self['next_page']
        self['offset_prev_page'] = self['limit'] * self['prev_page']
        self['offset_last_page'] = self['limit'] * (self['num_pages'] - 1)

        self['curr_page'] += 1
        self['next_page'] += 1
        self['prev_page'] += 1

        items = {}
        if self['params']:
            for key, val in self['params'].iteritems():
                if isinstance(val, str) or isinstance(val, unicode):
                    items[key] = val.encode('utf-8')
                else:
                    items[key] = val

            self['url_params'] = urllib.urlencode(items)

        self['params'] = items

        self['is_last'] = (self['offset'] + self['limit']) >= self['num_rows']

        self['page_range'] = list(self.page_xrange) if self[
            'is_range'] else None

    def _page(self, i):
        offset = (i - 1) * self['limit']
        params = dict(self['params'])
        params[self['offset_name']] = offset

        return {
            'n': i,
            'o': offset,
            'url_params': urllib.urlencode(params),
            'is_current': self['curr_page'] == i
        }

    @property
    def page_xrange(self):
        return self._get_page_range()

    @property
    def first_page(self):
        return self._page(1)

    @property
    def last_page(self):
        return self._page(self['num_pages'])

    @property
    def next_page(self):
        return self._page(self['next_page'])

    @property
    def curr_page(self):
        return self._page(self['curr_page'])

    @property
    def prev_page(self):
        return self._page(self['prev_page'])

    def _get_page_range(self):
        num_pages = self['num_pages']
        splitter = {'is_splitter': True,
                    'splitter': self['splitter']}
        view_num = self['view_num']
        curr_page = self['curr_page']

        if num_pages <= view_num:
            for i in xrange(1, num_pages + 1):
                yield self._page(i)

        else:
            df = view_num / 2
            if curr_page <= df:
                for i in xrange(1, view_num):
                    yield self._page(i)

                yield splitter
            elif curr_page >= num_pages - df:
                yield self._page(1)
                yield splitter

                for i in xrange(num_pages - view_num + 2, num_pages):
                    yield self._page(i)

            else:
                yield self._page(1)

                if curr_page != df + 1:
                    yield splitter

                for i in xrange(curr_page - df + 1, curr_page + df):
                    yield self._page(i)

                if curr_page != num_pages - df:
                    yield splitter

            yield self._page(num_pages)
